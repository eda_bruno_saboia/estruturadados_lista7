package br.ufc.list7.q1

trait IUnionFind {
  def createSet(): Unit
  def find(v: Int): Int
  def makeSet()
  def union(a: Int, b: Int): Boolean
  def sameSet(a: Int, b: Int): Boolean
  def free()
  def printSet()
}