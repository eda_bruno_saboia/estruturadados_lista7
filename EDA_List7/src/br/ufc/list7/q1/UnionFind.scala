package br.ufc.list7.q1

class UnionFind(n: Int) extends IUnionFind{
  var values: Array[Element] = new Array[Element](n)
  
  override def createSet(): Unit ={
		for(i<-0 until values.length){
			values(i) = new Element()
		}
	}
  
	override def find(v: Int): Int ={ 
    for(i<-0 until values.length){
		  var x = values(i)
      if(x.value == v) 
				return x.root
    }	
		return -2
    
	}   
  
  override def makeSet(){
    for(i<-0 until values.length){
			var x = values(i)
			x.root = x.value
    }
  }
  
  override def union(a: Int, b: Int): Boolean ={
    var rootA = find(a)
    for(i<-0 until values.length){
      if(values(i).value == b){
        values(i).root = rootA
        return true
      }
    }
    return false
  }
  
  override def sameSet(a: Int, b: Int): Boolean ={
    return find(a) == find(b)
  }
  
  override def free(){
      this.values = null
    }
  
  override def printSet(): Unit={
    for (x<- values){
      print(x.value)
    }
    println
  }
    
}