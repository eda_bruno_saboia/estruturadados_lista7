package br.ufc.list7.q1

object Main {
  
  def main(args: Array[String]) {   
  
    var n: Int = 5
    var unionFind = new UnionFind(n)
    unionFind.createSet()
    
    println("Inserir os elementos de 0 - 4")
    var elements = new Array[Element](n)
        for(i<-0 until 5){
          elements(i) = new Element()
          elements(i).value = i
        }
        
    unionFind.values = elements    
        
    unionFind.makeSet()
    unionFind.printSet()
    
    println("Procurando o elemento 0")
    println(unionFind.find(0))
    println("Procurando o elemento 1")
    println(unionFind.find(1))
    println("Procurando o elemento 2")
    println(unionFind.find(2))
    println("Procurando o elemento 3")
    println(unionFind.find(3))
    println("Procurando o elemento 4")
    println(unionFind.find(4))
    
    println("Unindo 0 e 1") 
    unionFind.union(0, 1)
    println("Unindo 0 e 2")
    unionFind.union(0, 2)
    println("Unindo 0 e 3")
		unionFind.union(0, 3)
    
		println("Verificando se o elementos 0 e 1 estao no mesmo conjunto")
		unionFind.sameSet(0, 1) match {
          case true => println("Elementos do mesmo conjunto")
          case false => println("Elementos de conjuntos diferentes")
        }
    
    println("Verificando se o elementos 0 e 4 estao no mesmo conjunto")
    unionFind.sameSet(0, 4) match {
      case true => println("Elementos do mesmo conjunto")
      case false => println("Elementos de conjuntos diferentes")
    }
		
		unionFind.free();
		
}
}